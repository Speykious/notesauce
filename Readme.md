# Note sauce

> This is where I upload my project files for my noteblock covers, when I feel like sharing them.

Any `.mmpz` file is a project that you can open in LMMS.

If you're having trouble loading the project (missing sound files or something)
it's probably because you don't have every sound in the exact same place I had them.

Maybe later I could find a way to make that better, but meh, for now I'm lazy. c:

![idk some enderman and some piglin](https://i.imgur.com/vIzbOKo.jpg)

## License

Copyright © 2021 Speykious <speykious@gmail.com> (and Various Artists)

Every project file uploaded to this repository falls under the [Creative Commons Attribution-ShareAlike 4.0 International Public License](https://creativecommons.org/licenses/by-sa/4.0/).
See [legal details here](https://creativecommons.org/licenses/by-sa/4.0/legalcode).

As long as you follow this license, you can use my project files in the way you like without asking me anything.
Though don't hesitate to do so if you have any doubts!